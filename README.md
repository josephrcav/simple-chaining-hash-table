# Simple Chaining Hash Table

My Hash Table project for my students at Universidad Carlos III de Madrid for
subject Algorhitms, which I proudly teach for 6 years now.
It is very simple hash table that uses chaining and DJB hash function.

useful links to help understand this implementation:

https://en.wikipedia.org/wiki/Hash_table

http://www.cse.yorku.ca/~oz/hash.html

It is free to edit, reuse and distribute this code.
